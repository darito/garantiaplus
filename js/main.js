 $(document).ready(function () {
     $(".owl-carousel").owlCarousel({
         items: 1,
         loop: true,
         autoplay: true,
         autoplayTimeout: 6000,
     });
 });

 function colocaAltoAlIframe() {
    $('iframe#video').css('height', $(window).height())
}
$(window).ready(colocaAltoAlIframe)
$( window ).resize(colocaAltoAlIframe)


 // Añade clase `affix-top` o `affix` después de escrollear 
 $('#mainNav').affix({
     offset: {
         top: 100
     }
 })